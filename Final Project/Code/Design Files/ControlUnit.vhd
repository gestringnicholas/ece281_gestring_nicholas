--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : ControlUnit.vhd
--| AUTHOR(S)     : Capt Dan Johnson
--| CREATED       : 05/04/2020
--| DESCRIPTION   : This file connects the ALUDecoder and the Main Decoder
--| together.  It takes a opcode and a Funct input and produces all
--| of the control signals the MIPS Single-Cycle Processor requires.  These 
--| signals in
--|
--|		Inputs:   Funct(5:0)     --> 5-bit signal for R-type instructions
--|				  Opcode(5:0)    --> 5-bit signal to determine instruction type				  							
--|		 
--|		Outputs:  RegDst         --> determines write register (rs/rt)
--|				  Branch         --> signal for branch instructions
--|				  MemtoReg       --> controls if data is going from memory to register
--|				  Jump	         --> signal for jump instructions
--|				  MemWrite       --> enables writing to memory
--|				  ALUSrc         --> controls if data is from RD2 or SignImm
--|				  RegWrite       --> enables writing to register
--|				  ALUControl(2:0)--> determions ALU function
--|
--| DOCUMENTATION : Modeled after a lab provided with instructor notes for 
--| Digital Design and Computer Architecture, David Money Harris & Sarah L. 
--| Harris, 2nd Edition
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : MainDecoder.vhd, ALUDecoder.vhd
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library unisim;
  use UNISIM.Vcomponents.ALL;
  
entity ControlUnit is 
  port(
	Opcode    : in std_logic_vector(5 downto 0);
	Funct	  : in std_logic_vector(5 downto 0);
	RegDst    : out std_logic;
	Branch    : out std_logic;
	MemtoReg  : out std_logic;
	Jump	  : out std_logic;
	MemWrite  : out std_logic;
	ALUSrc    : out std_logic;
	RegWrite  : out std_logic;
	ALUControl: out std_logic_vector(2 downto 0)
  );
end ControlUnit;

architecture ControlUnit_arch of ControlUnit is 

	component ALUDecoder is 
	port(
		ALUOp : in std_logic_vector(1 downto 0);
		Funct : in std_logic_vector(5 downto 0);
		ALUControl: out std_logic_vector(2 downto 0)
	);
	end component;

	component MainDecoder is 
	port(
		Opcode   : in std_logic_vector(5 downto 0);
		RegDst   : out std_logic;
		Branch   : out std_logic;
		MemtoReg : out std_logic;
		Jump	 : out std_logic;
		ALUOp    : out std_logic_vector(1 downto 0);
		MemWrite : out std_logic;
		ALUSrc   : out std_logic;
		RegWrite : out std_logic
	);
	end component;
	
	signal w_ALUOp : std_logic_vector(1 downto 0);
  
begin
	-- PORT MAPS ----------------------------------------
	
	mainDec_inst : MainDecoder port map (
		Opcode   => Opcode,
        RegDst   => RegDst,
        Branch   => Branch,
        MemtoReg => MemtoReg,
        Jump	 => Jump,
        ALUOp    => w_ALUOp,
        MemWrite => MemWrite,
        ALUSrc   => ALUSrc,
        RegWrite => RegWrite
	);
	
	ALUDec_inst : ALUDecoder port map (
		ALUOp 		=>  w_ALUOp,
		Funct   	=>  Funct,
		ALUControl  => ALUControl
	);
	-----------------------------------------------------
end ControlUnit_arch;
