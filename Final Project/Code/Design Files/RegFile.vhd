--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : RegFile.vhd
--| AUTHOR(S)     : Capt Dan Johnson
--| CREATED       : 04/02/2020
--| DESCRIPTION   : This file implements a simple register file. Each register
--| is addresable with a 5-bit address.  It contains three address ports.  Two
--| ports are for reading and will output the contents of the register to RD1 and
--| RD2. A3 will address a register to write to.  If WE3 is '1' the value at WD3
--| will be written to the register indicated by A3.  The clk is only used to 
--| regulate the write function.
--|
--|		Inputs:   clk 	 	--> simulated clock
--| 	          A1(4:0)	--> 5-bit address for RD1 register
--| 	          A2(4:0)	--> 5-bit address for RD3 register
--| 	          A3(4:0)	--> 5-bit address for WD3 register
--| 	          WE3    	--> Write Enable
--|				  WD3(31:0)	--> Data to be written to register at A3
--|				  
--|		Outputs:  RD1(31:0) --> Data from register address A1
--|		          RD2(31:0) --> Data from register address A2
--|
--| DOCUMENTATION : Modeled after a lab provided with instructor notes for 
--| Digital Design and Computer Architecture, David Money Harris & Sarah L. 
--| Harris, 2nd Edition
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : None
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  use IEEE.STD_LOGIC_UNSIGNED.all;

library unisim;
  use UNISIM.Vcomponents.ALL;

entity RegFile is 
  port(
		clk	: in std_logic;
		A1 	: in std_logic_vector(4 downto 0);
		A2  : in std_logic_vector(4 downto 0);
		A3  : in std_logic_vector(4 downto 0);
		WE3 : in std_logic;
		WD3 : in std_logic_vector(31 downto 0);
		RD1 : out std_logic_vector(31 downto 0);
		RD2 : out std_logic_vector(31 downto 0)		
  );
end RegFile;

architecture RegFile_arch of RegFile is 

	type regType is array (31 downto 0) of std_logic_vector(31 downto 0);
	signal registers : regType;
  
begin

	-- CONCURRENT STATEMENTS "MODULES" ------------------

	-- RD1 and RD2 output the contents at A1 and A2 -----
	RD1  <= x"00000000" when A1="00000" else
			registers(conv_integer(A1));
	RD2  <= x"00000000" when A2="00000" else
			registers(conv_integer(A2));		
	-----------------------------------------------------


	-- PROCESSES ----------------------------------------			
			
	-- Write WD3 to reg at A3 when rising edge and WE='1'
	write_process : process(clk)
	begin
	if rising_edge(clk) then
	if(WE3='1') then registers(conv_integer(A3))<= WD3;
	end if;
	end if;
	end process;
	-----------------------------------------------------
			
end RegFile_arch;
