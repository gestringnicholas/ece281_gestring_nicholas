--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : MainDecoder.vhd
--| AUTHOR(S)     : Capt Dan Johnson
--| CREATED       : 05/04/2020
--| DESCRIPTION   : This file implements the Main Decoder portion of the control
--| unit.  It takes in the 6-bit opcode and determines all control signals except
--| the ALUControl
--|
--|		Inputs:	  Opcode(5:0)    --> 5-bit signal to determine instruction type				  							
--|		 
--|		Outputs:  RegDst         --> determines write register (rs/rt)
--|				  Branch         --> signal for branch instructions
--|				  MemtoReg       --> controls if data is going from mem to reg
--|				  Jump	         --> signal for jump instructions
--|				  MemWrite       --> enables writing to memory
--|				  ALUSrc         --> controls if data is from RD2 or SignImm
--|				  RegWrite       --> enables writing to register
--|				  ALUOp(1:0)	 --> sent to ALUDecoder to determine ALUControl
--|
--| DOCUMENTATION : Modeled after a lab provided with instructor notes for 
--| Digital Design and Computer Architecture, David Money Harris & Sarah L. 
--| Harris, 2nd Edition
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : None
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library unisim;
  use UNISIM.Vcomponents.ALL;
 
entity MainDecoder is 
  port(
	Opcode   : in std_logic_vector(5 downto 0);
	RegDst   : out std_logic;
	Branch   : out std_logic;
	MemtoReg : out std_logic;
	Jump	 : out std_logic;
	ALUOp    : out std_logic_vector(1 downto 0);
	MemWrite : out std_logic;
	ALUSrc   : out std_logic;
	RegWrite : out std_logic
  );
end MainDecoder;

architecture MainDecoder_arch of MainDecoder is 

signal controls : std_logic_vector(8 downto 0);
  
begin

	-- CONCURRENT STATEMENTS "MODULES" -----------------
														
	-- Control signals LUT -----------------------------
	with Opcode select                                  
	controls <= "110000010" when "000000", -- R-type    
				"101001000" when "100011", -- LW        
				"001010000" when "101011", -- SW        
				"000100001" when "000100", -- BEQ       
				"101000000" when "001000", -- ADDi      
				"000000100" when "000010", -- J         
				"---------" when others;   -- illegal op
	----------------------------------------------------

	-- Assigning control vector to control signals -----
		RegWrite <= controls(8);
		RegDst   <= controls(7);
		ALUSrc   <= controls(6);
		Branch   <= controls(5);
		MemWrite <= controls(4);
		MemtoReg <= controls(3);
		Jump     <= controls(2);
		ALUOp    <= controls(1 downto 0);
	----------------------------------------------------
	
end MainDecoder_arch;
