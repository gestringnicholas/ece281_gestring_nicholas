--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : ALUDecoder.vhd
--| AUTHOR(S)     : Capt Dan Johnson
--| CREATED       : 05/04/2020
--| DESCRIPTION   : This file implements an ALU decoder.  It takes in the 
--|	ALUOp(1:0) and Function(5:0) signals and implements look up tables to  
--| determine the appropriate ALUControl(2:0) output According to the table below	

--|
--|		Inputs:   ALUop(1:0)   		--> 2-bit ALU op code to determine function
--| 	          Funct(5:0)   		--> 5-bit signal for R-type instructions
--|				 
--|		Outputs:  ALUControl(2:0)   --> 3 bit contol signal.  See table
--|		
--|		| ALUControl2:0 | Function |
--|		|---------------|----------|
--|		|      000      | A & B    |
--|		|      001      | A | B    |
--|		|      010      | A + B    |
--|		|      011      | not used |
--|		|      100      | A & ~B   |
--|		|      101      | A | ~B   |
--|		|      110      | A - B    |
--|		|      111      | SLT      |				          
--|
--| DOCUMENTATION : Modeled after a lab provided with instructor notes for 
--| Digital Design and Computer Architecture, David Money Harris & Sarah L. 
--| Harris, 2nd Edition
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : None
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library unisim;
  use UNISIM.Vcomponents.ALL;
  
entity ALUDecoder is 
  port(
	ALUOp : in std_logic_vector(1 downto 0);
	Funct : in std_logic_vector(5 downto 0);
	ALUControl: out std_logic_vector(2 downto 0)
  );
end ALUDecoder;

architecture ALUDecoder_arch of ALUDecoder is 

signal functSelect : std_logic_vector(2 downto 0);
  
begin

	-- CONCURRENT STATEMENTS "MODULES" --------------------------
																 
    -- Look up table to select ALUControl(2:0) given ALUOp(1:0) -
	with ALUOp select                                            
	ALUControl <= "010" when "00", --add (for lw/sw/addi)        
				  "110" when "01", --sub (for beq)               
				  "001" when "11", --OR  (for ORi)               
				  functSelect when others;                       
    -------------------------------------------------------------
																 
	-- LUT for ALUControl(2:0) given Funct(5:0) & ALUOp = "10" --   
	with Funct select                                            
	functSelect <= "010" when "100000", -- add (for add)         
				   "110" when "100010", -- subtract (for sub)    
				   "000" when "100100", -- logical and (for and) 
				   "001" when "100101", -- logical or (for or)   
				   "111" when "101010", -- set on less (for slt) 
				   "010" when others;                            
    -------------------------------------------------------------
	
end ALUDecoder_arch;
