--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : alu.vhd
--| AUTHOR(S)     : Capt Phillip Warner
--| CREATED       : 02/08/2017
--| DESCRIPTION   : This file implements a 32-bit ALU with 7 functions as described 
--| 				below.
--|
--|		Inputs:   a(31:0) --> First 31-bit input value
--| 	          b(31:0) --> Second 31-bit input value
--| 	          f(2:0)  --> 3-bit function select defined by table below
--|				  
--|		Outputs:  y(31:0) --> 31-bit output containing ALU result
--|		          zero    --> Flag indicated is '1' if ALU result is zero
--|
--|					| F2:0 | Function |
--|					|------|----------|
--|					| 000  | A & B    |
--|					| 001  | A | B    |
--|					| 010  | A + B    |
--|					| 011  | not used |
--|					| 100  | A & ~B   |
--|					| 101  | A | ~B   |
--|					| 110  | A - B    |
--|					| 111  | SLT      |
--|
--| DOCUMENTATION : None
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, std_logic_signed
--|    Files     : None
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  use ieee.std_logic_signed.all; -- required for '+' operator

entity alu is 
  port(
	a, b	:	in 	std_logic_vector(31 downto 0);
	f		:	in	std_logic_vector(2 downto 0);
	y		:	out	std_logic_vector(31 downto 0);
	zero	:	out	std_logic
  );
end alu;

architecture alu_arch of alu is 
	
	signal c_B_mux2, c_sign, c_sum, c_or, c_and, c_Y_mux4 	: std_logic_vector(31 downto 0) := (others => '0');
	signal c_result	: std_logic_vector(32 downto 0) := (others => '0'); -- holds adder raw sum
	signal c_out	: std_logic := '0';
  
begin
	-- CONCURRENT STATEMENTS "MODULES" ------------------
		
	-- B 2 to 1 MUX -------------------------------------
	c_B_mux2 <= b when f(2) = '0' else not b;
	-----------------------------------------------------
	
	-- Basic ALU function for Add, OR, and AND ----------
	c_result  <= ("0" & a) + ("0" & c_B_mux2) + f(2);
	c_sum	  <= c_result(31 downto 0);
	c_out	  <= c_result(32);	-- not used for now
	-----------------------------------------------------

	-- Basic ALU function for OR and AND ----------------	
	c_or   <= a or c_B_mux2;
	c_and  <= a and c_B_mux2;
	-----------------------------------------------------
	
	-- Zero Extend 1 bit to 32 bits ---------------------
	c_sign <= x"0000000" & "000" & c_sum(31);
	-----------------------------------------------------
	
	-- Y 4 to 1 MUX -------------------------------------
	c_Y_mux4 <= c_and when f(1 downto 0) = "00" else
	            c_or  when f(1 downto 0) = "01" else
	            c_sum when f(1 downto 0) = "10" else
	            c_sign;
	-----------------------------------------------------
	
	y <= c_Y_mux4;
	
	-- ZERO bit -----------------------------------------
	zero <= '1' when c_Y_mux4 = x"00000000" else '0';
	-----------------------------------------------------
	
end alu_arch;
