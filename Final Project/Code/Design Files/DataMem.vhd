--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : DataMem.vhd
--| AUTHOR(S)     : Capt Dan Johnson
--| CREATED       : 04/02/2020
--| DESCRIPTION   : This file implements a simple data memory component.  It is
--| word addressable and can store 256 words.  When reading in an address it
--| only takes bits 9 down to 2 which provides a 6 bit address from a full 32-bit
--| address.
--|
--|		Inputs:   clk 		--> simulated clock
--|				  A (31:0)	--> Address to read or write to memory (9:2) used
--|				  WE		--> Write Enable
--|				  WD(31:0)	--> Data to be written	  							
--|		   
--|		Outputs:  RD(31:0)	--> Data from address A
--|
--| DOCUMENTATION : Modeled after a lab provided with instructor notes for 
--| Digital Design and Computer Architecture, David Money Harris & Sarah L. 
--| Harris, 2nd Edition
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : None
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  use IEEE.STD_LOGIC_UNSIGNED.all;

library unisim;
  use UNISIM.Vcomponents.ALL;

entity DataMem is 
  port(
		clk	: in std_logic;
		A 	: in std_logic_vector(31 downto 0);
		WE  : in std_logic;
		WD  : in std_logic_vector(31 downto 0);
		RD  : out std_logic_vector(31 downto 0)
  );
end DataMem;

architecture DataMem_arch of DataMem is 

   type ramtype is array (255 downto 0) of STD_LOGIC_VECTOR(31 downto 0);
   signal mem: ramtype := (others=>x"ECE28100");
  
begin

	-- CONCURRENT STATEMENTS "MODULES" ------------------
	
	-- Reading contents from address --------------------
	RD <= mem(conv_integer(A(9 downto 2)));
	-----------------------------------------------------	
	
	
	-- PROCESSES ----------------------------------------

	-- Write WD to address from A if rising edge & WE='1'
	write_process : process(clk)
	begin
	if(rising_edge(clk)) then
	if(WE='1') then mem(conv_integer(A(9 downto 2)))<= WD;
	end if;
	end if;
	end process;
	-----------------------------------------------------
	
end DataMem_arch;
