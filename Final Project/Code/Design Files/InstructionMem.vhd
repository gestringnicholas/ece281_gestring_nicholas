--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : InstructionMem.vhd
--| AUTHOR(S)     : Capt Dan Johnson
--| CREATED       : 04/02/2020
--| DESCRIPTION   : This file implements a simple instruction memory component.
--| It is word addressable and can store 32 words.  When reading in an address it
--| only takes bits 6 down to 2 which provides a 5 bit address from a full 32-bit
--| address.  This file must be modified to list the instructions the user intends
--| to implement.
--|
--|		Inputs:   clk 		--> simulated clock
--|				  A (31:0)	--> Address to read to memory (6:2) used  							
--|		   
--|		Outputs:  RD(31:0)	--> Data from address A
--|
--| DOCUMENTATION : Modeled after a lab provided with instructor notes for 
--| Digital Design and Computer Architecture, David Money Harris & Sarah L. 
--| Harris, 2nd Edition
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : None
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  use IEEE.STD_LOGIC_UNSIGNED.all;

library unisim;
  use UNISIM.Vcomponents.ALL;
 
entity InstructionMem is 
  port(
		clk	: in std_logic;
		A 	: in std_logic_vector(31 downto 0);
		RD : out std_logic_vector(31 downto 0)
  );
end InstructionMem;

architecture InstructionMem_arch of InstructionMem is 

   type ramtype is array (31 downto 0) of STD_LOGIC_VECTOR(31 downto 0);
   signal mem: ramtype := (others=>x"00000000");
   
begin

	-- CONCURRENT STATEMENTS "MODULES" ------------------
	
	-- Assigning initial instructions to mem ------------
	mem(0) <= x"20020005"; --00
    mem(1) <= x"2003000c"; --04
    mem(2) <= x"2067fff7"; --08
    mem(3) <= x"00e22025"; --0c
    mem(4) <= x"00642824"; --10
    mem(5) <= x"00a42820"; --14
    mem(6) <= x"10a7000a"; --18
    mem(7) <= x"0064202a"; --1c
    mem(8) <= x"10800001"; --20
    mem(9) <= x"20050000"; --24
    mem(10) <= x"00e2202a"; --28
    mem(11) <= x"00853820"; --2c
    mem(12) <= x"00e23822"; --30
    mem(13) <= x"ac67001e"; --34
    mem(14) <= x"8c02002a"; --38
    mem(15) <= x"08000011"; --3c
    mem(16) <= x"20020001"; --40
    mem(17) <= x"ac020032"; --44


	-- ***Finish assigning all of the instructions***
	
	-----------------------------------------------------
	
	-- RD outputs value stored at A(6 downto 2) ---------
	RD <= mem(conv_integer(A(6 downto 2)));
	-----------------------------------------------------
	
end InstructionMem_arch;
