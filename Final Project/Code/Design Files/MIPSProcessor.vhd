--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : MIPSProcessor.vhd
--| AUTHOR(S)     : Capt Dan Johnson
--| CREATED       : 04/01/2020
--| DESCRIPTION   : This file is the template for the top level component to 
--| implement the Single Cycle Mips processor for the ECE 281 final project.
--| It brings together the Instruction Memory, Register File, Control Unit, and
--| Data Memory with muxes, adders, shifters and registers to complete the
--| processor.  Instructions are written into the Instruction Memory component
--| and are processed until all are complete.  It is capable of executing the
--| add, sub, and, or, slt, lw, sw, beq, addi, and j instructions.
--|
--|		Inputs:   clk   --> simulated clock
--|				  reset	--> used to set instruction memory to first address 
--|
--|		Outputs:  none							
--|
--| DOCUMENTATION : Modeled after a lab provided with instructor notes for 
--| Digital Design and Computer Architecture, David Money Harris & Sarah L. 
--| Harris, 2nd Edition 
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std,  std_logic_signed, unisim
--|    Files     : RegFile.vhd, InstructionMem.vhd, DataMem.vhd, alu.vhd, 
--|	   ControlUnit.vhd (MainDecoder.vhd, AlUDecoder.vhd)
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;  
  use ieee.std_logic_signed.all; -- required for '+' operator

library unisim;
  use UNISIM.Vcomponents.ALL;

entity MIPSProcessor is 
  port(
	clk   : in std_logic;
	reset : in std_logic
  );
end MIPSProcessor;

architecture MIPSProcessor_arch of MIPSProcessor is 

-- Declare all components you will use in this file

    component InstructionMem is 
      port(
            clk    : in std_logic;
            A      : in std_logic_vector(31 downto 0);
            RD     : out std_logic_vector(31 downto 0)
      );
    end component InstructionMem;
    
    component ControlUnit is 
      port(
        Opcode     : in std_logic_vector(5 downto 0);
        Funct      : in std_logic_vector(5 downto 0);
        RegDst     : out std_logic;
        Branch     : out std_logic;
        MemtoReg   : out std_logic;
        Jump       : out std_logic;
        MemWrite   : out std_logic;
        ALUSrc     : out std_logic;
        RegWrite   : out std_logic;
        ALUControl : out std_logic_vector(2 downto 0)
      );
    end component ControlUnit;
     
    component RegFile is 
      port(
            clk    : in std_logic;
            A1     : in std_logic_vector(4 downto 0);
            A2     : in std_logic_vector(4 downto 0);
            A3     : in std_logic_vector(4 downto 0);
            WE3    : in std_logic;
            WD3    : in std_logic_vector(31 downto 0);
            RD1    : out std_logic_vector(31 downto 0);
            RD2    : out std_logic_vector(31 downto 0)        
      );
    end component RegFile;
    
    component alu is 
        port(
           a, b    :    in     std_logic_vector(31 downto 0);
           f       :    in    std_logic_vector(2 downto 0);
           y       :    out    std_logic_vector(31 downto 0);
           zero    :    out    std_logic
         );
    end component alu;    
    
    component DataMem is 
        port(
            clk    : in std_logic;
            A      : in std_logic_vector(31 downto 0);
            WE     : in std_logic;
            WD     : in std_logic_vector(31 downto 0);
            RD     : out std_logic_vector(31 downto 0)
        );
    end component DataMem;
    
-- Create components from mux entities
    
    component mux_SrcB is
        port(
            SEL : in std_logic; -- select input
            zero_choice   : in std_logic_vector (31 downto 0); -- inputs
            one_choice    : in std_logic_vector (31 downto 0); -- inputs            
            Y   : out std_logic_vector (31 downto 0)
        );
    end component mux_SrcB;
    
    component mux_Result is
        port(
            SEL2 : in std_logic; -- select input
            zero_choice2   : in std_logic_vector (31 downto 0); -- inputs
            one_choice2    : in std_logic_vector (31 downto 0); -- inputs            
            Y2   : out std_logic_vector (31 downto 0)
        );
    end component mux_Result;
      
    component mux_PCSrc is
        port(
            SEL3 : in std_logic; -- select input
            zero_choice3   : in std_logic_vector (31 downto 0); -- inputs
            one_choice3    : in std_logic_vector (31 downto 0); -- inputs            
            Y3   : out std_logic_vector (31 downto 0)
        );
    end component mux_PCSrc;
      
    component mux_Jump is
        port(
            SEL4 : in std_logic; -- select input
            zero_choice4   : in std_logic_vector (31 downto 0); -- inputs
            one_choice4    : in std_logic_vector (31 downto 0); -- inputs            
            Y4  : out std_logic_vector (31 downto 0)
        );
    end component mux_Jump;
    
    component mux_WriteReg is
        port(
            SEL5 : in std_logic; -- select input
            zero_choice5   : in std_logic_vector (4 downto 0); -- inputs
            one_choice5    : in std_logic_vector (4 downto 0); -- inputs            
            Y5  : out std_logic_vector (4 downto 0)
        );
    end component mux_WriteReg;

-- Declare all signals you will need to use (as indicated in MIPS Single Cycle Figure)
-- Remember you will need signals for all connections besides the signals in the entity
  
  signal w_Jump, w_MemtoReg, w_Memwrite, w_Branch, w_PCSrc, w_ALUSrc, w_RegDst, w_RegWrite, w_zero : std_logic;
  signal w_ALUControl  : std_logic_vector(2 downto 0);
  signal w_Op, w_funct : std_logic_vector(5 downto 0);
  signal w_signExt     : std_logic_vector(15 downto 0);
  signal w_leftshift1   : std_logic_vector(25 downto 0);
  signal w_leftshifted1 : std_logic_vector(27 downto 0);
  signal w_0, w_1, w_WriteReg, w_A1, w_A2 : std_logic_vector(4 downto 0);
  signal w_Instr, w_PC, w_Result, w_RD1, w_RD2, w_SrcB, w_ALUResult, w_ReadData, w_ImmExt, w_PCSrc_mux, w_PCJump, w_PCPrime, w_PCPlus4, w_PCBranch, w_leftshift2, w_leftshifted2 : std_logic_vector(31 downto 0);
  
begin
	-- PORT MAPS ----------------------------------------
	-- Port map all declared components
    
    InstructMem_inst : InstructionMem
        port map(
            clk => clk,
            A => w_PC,
            RD => w_Instr
        );
        
    ControlUnit_inst : ControlUnit
        port map(
            Opcode => w_Op,
            Funct => w_Funct,
            RegDst => w_RegDst,
            Branch => w_Branch,
            MemtoReg => w_MemtoReg,
            Jump => w_Jump,
            MemWrite => w_MemWrite,
            ALUSrc => w_ALUSrc,
            RegWrite => w_RegWrite,
            ALUControl => w_ALUControl
        );

    RegFile_inst : RegFile
        port map(
            clk => clk,
            A1 => w_A1,
            A2 => w_A2,
            A3 => w_WriteReg,
            WE3 => w_RegWrite,
            WD3 => w_Result,
            RD1 => w_RD1,
            RD2 => w_RD2
        );
        
    alu_inst : alu
        port map(
            a => w_RD1,
            b => w_SrcB,
            f => w_ALUControl,
            y => w_ALUResult,
            zero => w_zero
        );
        
    DataMem_inst : DataMem
        port map(
            clk => clk,
            A => w_ALUResult,
            WE => w_Memwrite,
            WD => w_RD2,
            RD => w_ReadData
        );
        
-- Port map muxes
        
    mux1_inst : mux_SrcB
        port map(
            SEL => w_ALUSrc,
            zero_choice => w_RD2,
            one_choice => w_ImmExt,
            Y => w_SrcB
        );
    
    mux2_inst : mux_Result
        port map(
            SEL2 => w_MemtoReg,
            zero_choice2 => w_ALUResult,
            one_choice2 => w_ReadData,
            Y2 => w_Result
        );      
        
    mux3_inst : mux_PCSrc
        port map(
            SEL3 => w_PCSrc,
            zero_choice3 => w_PCPlus4,
            one_choice3 => w_PCBranch,
            Y3 => w_PCSrc_mux
        );      
            
    mux4_inst : mux_Jump
        port map(
            SEL4 => w_Jump,
            zero_choice4 => w_PCSrc_mux,
            one_choice4 => w_PCJump,
            Y4 => w_PCPrime
        ); 
        
    mux5_inst : mux_WriteReg
        port map(
            SEL5 => w_RegDst,
            zero_choice5 => w_0,
            one_choice5 => w_1,
            Y5 => w_WriteReg
        );              
        
	-- CONCURRENT STATEMENTS "MODULES" ------------------												
	-- mplement all remaining concurrent logic
	
	w_Op <= w_Instr (31 downto 26);
    w_Funct <= w_Instr (5 downto 0);	
    w_A1 <= w_Instr (25 downto 21);
    w_A2 <= w_Instr (20 downto 16);
    w_0 <= w_Instr (20 downto 16);
    w_1 <= w_Instr (15 downto 11);
    w_signExt <= w_Instr (15 downto 0);
    w_leftshift1 <= w_Instr (25 downto 0);
    w_leftshift2 <= w_ImmExt;
    w_PCJump <= w_PCPlus4(31 downto 28) & w_leftshifted1;
        
    
    
	-- PROCESSES ----------------------------------------
	-- Implement any processes you need
	
	w_ImmExt <= (x"FFFF" & w_signExt) when (w_signExt(15) = '1') else
	            (x"0000" & w_signExt) when (w_signExt(15) = '0');
	            
	w_leftshifted1 <= w_leftshift1(25 downto 0) & "00";
	
	w_leftshifted2 <= w_leftshift2(29 downto 0) & "00";
	
	w_PCPlus4 <= w_PC + 4;
	
	w_PCBranch <= w_leftshifted2 + w_PCPlus4;
	
    program_counter_proc : process (clk, reset)
    begin
       if reset = '1' then
          w_PC <= x"00000000";
       elsif (rising_edge(clk)) then
          w_PC <= w_PCPrime;
       end if;

    end process program_counter_proc;
	   
end MIPSProcessor_arch;







--Create mux entities

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;  
  use ieee.std_logic_signed.all; -- required for '+' operator

library unisim;
  use UNISIM.Vcomponents.ALL;    
    
entity mux_SrcB is
    port(
        SEL : in std_logic; -- select input
        zero_choice   : in std_logic_vector (31 downto 0); -- inputs
        one_choice    : in std_logic_vector (31 downto 0); -- inputs            
        Y   : out std_logic_vector (31 downto 0)
    );
end mux_SrcB;
    
architecture Behavioral of Mux_SrcB is

begin
    Y <= zero_choice when (SEL = '0') else
         one_choice when (SEL = '1');
end Behavioral;

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;  
  use ieee.std_logic_signed.all; -- required for '+' operator

library unisim;
  use UNISIM.Vcomponents.ALL;    
    
entity mux_Result is
    port(
        SEL2 : in std_logic; -- select input
        zero_choice2   : in std_logic_vector (31 downto 0); -- inputs
        one_choice2    : in std_logic_vector (31 downto 0); -- inputs            
        Y2   : out std_logic_vector (31 downto 0)
    );
end mux_Result;
    
architecture Behavioral of Mux_Result is

begin
    Y2 <= zero_choice2 when (SEL2 = '0') else
          one_choice2 when (SEL2 = '1');

end Behavioral;

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;  
  use ieee.std_logic_signed.all; -- required for '+' operator

library unisim;
  use UNISIM.Vcomponents.ALL;    
    
entity mux_PCSrc is
    port(
        SEL3 : in std_logic; -- select input
        zero_choice3   : in std_logic_vector (31 downto 0); -- inputs
        one_choice3    : in std_logic_vector (31 downto 0); -- inputs            
        Y3   : out std_logic_vector (31 downto 0)
    );
end mux_PCSrc;
    
architecture Behavioral of Mux_PCSrc is

begin
    Y3 <= zero_choice3 when (SEL3 = '0') else
          one_choice3 when (SEL3 = '1');

end Behavioral;

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;  
  use ieee.std_logic_signed.all; -- required for '+' operator

library unisim;
  use UNISIM.Vcomponents.ALL;    
    
entity mux_Jump is
    port(
        SEL4 : in std_logic; -- select input
        zero_choice4   : in std_logic_vector (31 downto 0); -- inputs
        one_choice4    : in std_logic_vector (31 downto 0); -- inputs            
        Y4   : out std_logic_vector (31 downto 0)
    );
end mux_Jump;
    
architecture Behavioral of Mux_Jump is

begin
    Y4 <= zero_choice4 when (SEL4 = '0') else
          one_choice4 when (SEL4 = '1');

end Behavioral;

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;  
  use ieee.std_logic_signed.all; -- required for '+' operator

library unisim;
  use UNISIM.Vcomponents.ALL;    
    
entity mux_WriteReg is
    port(
        SEL5 : in std_logic; -- select input
        zero_choice5   : in std_logic_vector (4 downto 0); -- inputs
        one_choice5    : in std_logic_vector (4 downto 0); -- inputs            
        Y5   : out std_logic_vector (4 downto 0)
    );
end mux_WriteReg;
    
architecture Behavioral of Mux_WriteReg is

begin
    Y5 <= zero_choice5 when (SEL5 = '0') else
          one_choice5 when (SEL5 = '1');

end Behavioral;