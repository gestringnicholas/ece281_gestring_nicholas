--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : RegFile_tb.vhd (TEST BENCH)
--| AUTHOR(S)     : Capt Dan Johnson
--| CREATED       : 05/04/2020
--| DESCRIPTION   : This file tests to ensure RegFile functions properly
--|
--| DOCUMENTATION : Loop syntax found at
--| https://forums.xilinx.com/t5/Simulation-and-Verification/VHDL-Test-Bench-loop/td-p/508179
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : RegFile.vhd
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  use ieee.std_logic_signed.all;

library unisim;
  use UNISIM.Vcomponents.ALL;
  
entity RegFile_tb is
end RegFile_tb;

architecture test_bench of RegFile_tb is 
	
  component RegFile is
    port(
		clk	: in std_logic;
		A1 	: in std_logic_vector(4 downto 0);
		A2  : in std_logic_vector(4 downto 0);
		A3  : in std_logic_vector(4 downto 0);
		WE3 : in std_logic;
		WD3 : in std_logic_vector(31 downto 0);
		RD1 : out std_logic_vector(31 downto 0);
		RD2 : out std_logic_vector(31 downto 0)	
    );	
  end component;

	signal clk, WE3 : std_logic := '0';
	signal A1, A2, A3 : std_logic_vector(4 downto 0) := (others => '0');
	signal RD1, RD2, WD3: std_logic_vector(31 downto 0) := (others => '0');
	
	constant k_clk_period 	  : time := 10ns;
	
begin


	-- PORT MAPS ----------------------------------------

	uut_inst : RegFile port map (
		clk => clk,
		A1 	=> A1,
		A2  => A2,
		A3  => A3,
		WE3 => WE3,
		WD3 => WD3,
		RD1 => RD1,
		RD2 => RD2
	);

	-- PROCESSES ----------------------------------------------
	
    -- Clock process ------------------------------------------
    clk_proc : process
    begin
 		clk <= '0';
 		wait for k_clk_period/2;
 		clk <= '1';
 		wait for k_clk_period/2;
    end process clk_proc;

	-- Test Plan Process --------------------------------------
	test_process : process 
	begin
		A1 <= "00000";
		A2 <= "00001";
		A3 <= "00000";
		WD3 <= x"00400000"; WE3 <= '0';
	
	-- Checking that all registers are undef except 0 ---------
	for i in 0 to 15 loop
		WAIT FOR k_clk_period;
		A1 <= A1 + 2;
		A2 <= A2 + 2;		
	END LOOP;	
    -----------------------------------------------------------
    
    wait for k_clk_period; WE3<='1';	
    
 	-- Writing to reg starting at 0x00400000 and adding 4 ----- 	
	for i in 0 to 32 loop
        WAIT FOR k_clk_period;
        A3 <= A3 + 1;
        WD3<= WD3 + 4;
    END LOOP;    
    -----------------------------------------------------------    

    wait for k_clk_period; WE3<='0';    

	-- Checking all registers are written values except 0 -----
	for i in 0 to 15 loop
		WAIT FOR k_clk_period;
		A1 <= A1 + 2;
		A2 <= A2 + 2;		
	END LOOP;	
    -----------------------------------------------------------

	wait;
	end process;	

	
end test_bench;
