--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : DataMem_tb.vhd (TEST BENCH)
--| AUTHOR(S)     : Capt Dan Johnson
--| CREATED       : 05/04/2020
--| DESCRIPTION   : This file tests to ensure DataMem functions properly
--|
--| DOCUMENTATION : Loop syntax found at
--| https://forums.xilinx.com/t5/Simulation-and-Verification/VHDL-Test-Bench-loop/td-p/508179
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : DataMem.vhd
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  use ieee.std_logic_signed.all;
  
library unisim;
  use UNISIM.Vcomponents.ALL;
  
entity DataMem_tb is
end DataMem_tb;

architecture test_bench of DataMem_tb is 
	
  component DataMem is
    port(
		clk	: in std_logic;
		A 	: in std_logic_vector(31 downto 0);
		WE : in std_logic;
		WD : in std_logic_vector(31 downto 0);
		RD : out std_logic_vector(31 downto 0)	
    );	
  end component;

	signal clk, WE : std_logic := '0';
	signal A : std_logic_vector(31 downto 0) := (others => '0');
	signal RD, WD: std_logic_vector(31 downto 0) := (others => '0');
	
	constant k_clk_period 	  : time := 10ns;
	
begin


	-- PORT MAPS ----------------------------------------

	uut_inst : DataMem port map (
		clk => clk,
		A 	=> A,
		WE  => WE,
		WD  => WD,
		RD  => RD
	);



	-- PROCESSES ----------------------------------------------
	
    -- Clock process ------------------------------------------
    clk_proc : process
    begin
 		clk <= '0';
 		wait for k_clk_period/2;
 		clk <= '1';
 		wait for k_clk_period/2;
    end process clk_proc;
	
	-----------------------------------------------------
	
	-- Test Plan Process --------------------------------------
	
	test_process : process 
	begin
	
	--This test bench uses self checking.  Once run, search through
	--the TL Console  for ###.  This will highlight the messages.
	--If everything worked correctly there will be only two messages
	--posted to the console with no failures.
	
	A <= x"00000000";
    WD <= x"ECE28100";
    
	-- Checking that all data are 0xECE28100 ------------------
	for i in 0 to 255 loop
		WAIT FOR k_clk_period;
		A <= A + 4;
		ASSERT(RD<=x"ECE28100") REPORT "###Initial Values Test Failed###" SEVERITY ERROR;		
	END LOOP;	
    -----------------------------------------------------------

    REPORT "### All memory locations contain 0xECE28100###" SEVERITY note;
    WE<='1';
    
 	-- Writing to reg starting at 0x00400000 and adding 4 ----- 	
	for i in 0 to 255 loop
        WAIT FOR k_clk_period;
        WD <= WD + 1;
        A  <= A + 4;
    END LOOP;    
    ----------------------------------------------------------- 		

    WE<='0';A<=x"00000000"; WD<=x"ECE28100";
	-- Checking that all data are 0xECE28100 ------------------
	for i in 0 to 255 loop
		WAIT FOR k_clk_period;
		A <= A + 4;
		WD<= WD+1;
		ASSERT(RD<=WD) REPORT "###Write Vlaues Test Failed###" SEVERITY ERROR;		
	END LOOP;	
    -----------------------------------------------------------
  
    REPORT "### All memory locations contain new written values" SEVERITY note;  
		wait;
	end process;	
	-----------------------------------------------------------
	
end test_bench;
