--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : MIPSProcessor_tb.vhd (TEST BENCH)
--| AUTHOR(S)     : Capt Dan Johnson
--| CREATED       : 05/04/2020
--| DESCRIPTION   : This file tests to ensure MIPSProcessor functions properly
--|
--| DOCUMENTATION : None
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : MIPSProcessor.vhd
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library unisim;
  use UNISIM.Vcomponents.ALL;
  
entity MIPSProcessor_tb is
end MIPSProcessor_tb;

architecture test_bench of MIPSProcessor_tb is 
	
  component MIPSProcessor is
    port(
	clk :  in std_logic;
    reset : in std_logic
    );	
  end component;

	signal clk, reset : std_logic;
	constant k_clk_period : time := 10ns;
  
begin
	-- PORT MAPS ----------------------------------------

	uut_inst : MIPSProcessor port map (
	clk   => clk,
    reset => reset
	);

	
    -- Clock process ------------------------------------
    clk_proc : process
    begin
         clk <= '1';
         wait for k_clk_period/2;
         clk <= '0';
         wait for k_clk_period/2;
    end process clk_proc;
    -----------------------------------------------------
	
	
	-- Test Plan Process --------------------------------
	-- Initial reset then run until instructions done ---
	test_process : process 
	begin
	reset <= '1'; wait for 2 ns;
	reset <= '0';
	wait;
	end process;	
	-----------------------------------------------------	
	
end test_bench;
