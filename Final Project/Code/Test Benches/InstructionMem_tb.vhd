--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : InstructionMem_tb.vhd (TEST BENCH)
--| AUTHOR(S)     : Capt Dan Johnson
--| CREATED       : 05/04/2020
--| DESCRIPTION   : This file tests to ensure InstructionMem functions properly
--|
--| DOCUMENTATION : Loop syntax found at
--| https://forums.xilinx.com/t5/Simulation-and-Verification/VHDL-Test-Bench-loop/td-p/508179
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : InstructionMem.vhd
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  use ieee.std_logic_signed.all; -- required for '+' operator

library unisim;
  use UNISIM.Vcomponents.ALL;
  
entity InstructionMem_tb is
end InstructionMem_tb;

architecture test_bench of InstructionMem_tb is 
	
  component InstructionMem is
    port(
		clk	: in std_logic;
		A 	: in std_logic_vector(31 downto 0);
		RD : out std_logic_vector(31 downto 0)	
    );	
  end component;

	signal clk: std_logic := '0';
	signal A : std_logic_vector(31 downto 0) := (others => '0');
	signal RD: std_logic_vector(31 downto 0) := (others => '0');
	
	constant k_clk_period 	  : time := 10ns;
	
begin


	-- PORT MAPS ----------------------------------------

	uut_inst : InstructionMem port map (
		clk => clk,
		A 	=> A,
		RD  => RD
	);

	-- PROCESSES ----------------------------------------
	
    -- Clock process ------------------------------------
    clk_proc : process
    begin
 		clk <= '0';
 		wait for k_clk_period/2;
 		clk <= '1';
 		wait for k_clk_period/2;
    end process clk_proc;

	-- Test Plan Process --------------------------------
	test_process : process 
	begin
		-- Starting at 0x00000000 and iterating through
		-- all memory locations to verify loaded instructions
		A <= x"00000000";
	    WAIT FOR k_clk_period;
		for i in 0 to 30 loop
            WAIT FOR k_clk_period;
            A <= A + 4;
            WAIT FOR k_clk_period;
        END LOOP; 

		wait;
	end process;	
	-----------------------------------------------------	
	
end test_bench;
