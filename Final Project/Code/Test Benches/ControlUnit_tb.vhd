--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : ControlUnit_tb.vhd (TEST BENCH)
--| AUTHOR(S)     : Capt Dan Johnson
--| CREATED       : 05/04/2020
--| DESCRIPTION   : This file tests to ensure ControlUnit functions properly
--|
--| DOCUMENTATION : None
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : ControlUnit.vhd
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library unisim;
  use UNISIM.Vcomponents.ALL;
  
entity ControlUnit_tb is
end ControlUnit_tb;

architecture test_bench of ControlUnit_tb is 
	
  component ControlUnit is
    port(
		Opcode    : in std_logic_vector(5 downto 0);
		Funct	  : in std_logic_vector(5 downto 0);
		RegDst    : out std_logic;
		Branch    : out std_logic;
		MemtoReg  : out std_logic;
		Jump	  : out std_logic;
		MemWrite  : out std_logic;
		ALUSrc    : out std_logic;
		RegWrite  : out std_logic;
		ALUControl: out std_logic_vector(2 downto 0)
    );	
  end component;

	signal Opcode, Funct : std_logic_vector (5 downto 0);
	signal RegDst, Branch, MemtoReg, Jump, MemWrite, Regwrite, ALUSrc : std_logic;
	signal ALUControl : std_logic_vector(2 downto 0);

begin
	-- PORT MAPS --------------------------------------------------

	uut_inst : ControlUnit port map (
		Opcode     => Opcode,
        Funct	   => Funct,
        RegDst     => RegDst,
        Branch     => Branch,    
        MemtoReg   => MemtoReg,  
        Jump	   => Jump,	  
        MemWrite   => MemWrite,  
        ALUSrc     => ALUSrc,    
        RegWrite   => RegWrite,  
        ALUControl => ALUControl
	);
	
	-- Test Plan Process --------------------------------
	
	test_process : process 
	begin
	
	-- Providing inputs for every possible Opcode and Function combo
	-- for all possible instructions
		Opcode <= "001000"; Funct <= "------"; wait for 10 ns; -- Addi
		Opcode <= "000000"; Funct <= "100101"; wait for 10 ns; -- Or
		Opcode <= "000000"; Funct <= "100100"; wait for 10 ns; -- And
		Opcode <= "000000"; Funct <= "100000"; wait for 10 ns; -- Add
		Opcode <= "000000"; Funct <= "100010"; wait for 10 ns; -- Sub
		Opcode <= "000100"; Funct <= "001010"; wait for 10 ns; -- Beq
		Opcode <= "000000"; Funct <= "101010"; wait for 10 ns; -- slt
		Opcode <= "101011"; Funct <= "------"; wait for 10 ns; -- sw
		Opcode <= "100011"; Funct <= "------"; wait for 10 ns; -- lw
		Opcode <= "000010"; Funct <= "------"; wait for 10 ns; -- j
		wait;
		
	end process;	
	-----------------------------------------------------	
	
end test_bench;
