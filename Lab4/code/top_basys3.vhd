--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2018 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : top_basys3.vhd
--| AUTHOR(S)     : Capt Phillip Warner
--| CREATED       : 3/9/2018  MOdified by Capt Dan Johnson (3/30/2020)
--| DESCRIPTION   : This file implements the top level module for a BASYS 3 to 
--|					drive the Lab 4 Design Project (Advanced Elevator Controller).
--|
--|					Inputs: clk       --> 100 MHz clock from FPGA
--|							btnL      --> Rst Clk
--|							btnR      --> Rst FSM
--|							btnU      --> Rst Master
--|							btnC      --> GO (request floor)
--|							sw(15:12) --> Passenger location (floor select bits)
--| 						sw(3:0)   --> Desired location (floor select bits)
--| 						 - Minumum FUNCTIONALITY ONLY: sw(1) --> up_down, sw(0) --> stop
--|							 
--|					Outputs: led --> indicates elevator movement with sweeping pattern (additional functionality)
--|							   - led(10) --> led(15) = MOVING UP
--|							   - led(5)  --> led(0)  = MOVING DOWN
--|							   - ALL OFF		     = NOT MOVING
--|							 an(3:0)    --> seven-segment display anode active-low enable (AN3 ... AN0)
--|							 seg(6:0)	--> seven-segment display cathodes (CG ... CA.  DP unused)
--|
--| DOCUMENTATION : None
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std
--|    Files     : MooreElevatorController.vhd, clock_divider.vhd, sevenSegDecoder.vhd
--|				   thunderbird_fsm.vhd, sevenSegDecoder, TDM4.vhd, OTHERS???
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;


entity top_basys3 is
	port(

		clk     :   in std_logic; -- native 100MHz FPGA clock
		
		-- Switches (16 total)
		sw  	:   in std_logic_vector(15 downto 0);
		
		-- Buttons (5 total)
		btnC	:	in	std_logic;					  -- GO
		btnU	:	in	std_logic;					  -- master_reset
		btnL	:	in	std_logic;                    -- clk_reset
		btnR	:	in	std_logic;	                  -- fsm_reset
		--btnD	:	in	std_logic;			
		
		-- LEDs (16 total)
		led 	:   out std_logic_vector(15 downto 0);

		-- 7-segment display segments (active-low cathodes)
		seg		:	out std_logic_vector(6 downto 0);

		-- 7-segment display active-low enables (anodes)
		an      :	out std_logic_vector(3 downto 0)
	);
end top_basys3;

architecture top_basys3_arch of top_basys3 is 
  
	-- declare components and signals
    component MooreElevatorController is
        port ( clk2    : in  STD_LOGIC;
               reset   : in  STD_LOGIC;
               stop    : in  STD_LOGIC;
               up_down : in  STD_LOGIC;
               floor   : out STD_LOGIC_VECTOR (3 downto 0)     
             );
    end component MooreElevatorController;
    
    component clock_divider is
        generic ( constant k_DIV : natural := 2 ); -- How many clk cycles until slow clock toggles
                                                   -- Effectively, you divide the clk double this 
                                                   -- number (e.g., k_DIV := 2 --> clock divider of 4)
        port (  i_clk    : in std_logic;
                i_reset  : in std_logic;           -- asynchronous
                o_clk    : out std_logic           -- divided (slow) clock
        );
    end component clock_divider;
    
    component sevenSegDecoder is 
      port(
        -- Identify input and output bits here
        i_D : in std_logic_vector(3 downto 0);
        o_S : out std_logic_vector(6 downto 0)
      );
    end component sevenSegDecoder;
    
    component thunderbird_fsm is 
      port(
        -- Identify input and output bits here
        i_clk2,  i_reset2 : in std_logic;
        i_left, i_right : in std_logic;
        o_lights_L      : out std_logic_vector(5 downto 0);
        o_lights_R      : out std_logic_vector(5 downto 0)
      );
    end component thunderbird_fsm;
    
    component TDM4 is
        generic ( constant k_WIDTH : natural  := 4); -- bits in input and output
        Port ( i_CLK         : in  STD_LOGIC;
               i_RESET         : in  STD_LOGIC; -- asynchronous
               i_D3         : in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
               i_D2         : in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
               i_D1         : in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
               i_D0         : in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
               o_DATA       : out STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
               o_SEL        : out STD_LOGIC_VECTOR (3 downto 0)    -- selected data line (one-cold)
        );
    end component TDM4;
  
    signal w_fsmReset, w_clkReset, w_clk, w_up,  w_down, w_clk_tb, w_clk_tdm, i_go, o_activate, i_resetL, i_go2, o_stop, o_up_down, o_destAc, i_resetC : std_logic;
    signal c_floor, d_floor : std_logic_vector ( 3 downto 0);
    signal w_ones, w_tens : std_logic_vector ( 3 downto 0);
    signal o_sel, i_d : std_logic_vector (3 downto 0);
  
begin
	-- PORT MAPS ----------------------------------------
    elevator_inst : MooreElevatorController
        port map(
            reset => w_fsmReset,
            stop => o_stop,
            up_down => o_up_down,
            floor => c_floor,
            clk2 => w_clk            
        );
	
	clock_inst : clock_divider
	generic map( k_DIV => 25000000 )
	   port map(
	       i_reset => w_clkReset,
	       i_clk => clk,
	       o_clk => w_clk
	   );
	   
    clock_tb_inst : clock_divider
    generic map( k_DIV => 4750000 ) 
        port map(
           i_reset => w_clkReset,
           i_clk => clk,
           o_clk => w_clk_tb
       );
       
    clock_tdm_inst : clock_divider
    generic map( k_DIV => 100000 ) 
        port map(
           i_reset => w_clkReset,
           i_clk => clk,
           o_clk => w_clk_tdm
        );
	   
	SSD_inst : sevenSegDecoder
	    port map(
	       i_D => i_d,
	       o_S => seg
	    );
	    
	 thunderbird_fsm_inst : thunderbird_fsm
        port map (
           i_clk2 => w_clk_tb,
           i_reset2 => w_fsmReset,
           i_left => w_up,
           i_right => w_down,
           o_lights_L => led (15 downto 10),
           o_lights_R => led (5 downto 0)
        );
        
     TDM4_inst : TDM4
        port map (
            i_CLK => w_clk_tdm,
            i_RESET => btnU,
            i_D3 => w_ones,
            i_D2 => w_tens,
            i_D1 => w_ones,
            i_D0 => w_tens,
            o_DATA => i_d,
            o_SEL => o_sel
        );  
      
	
	-- CONCURRENT STATEMENTS ----------------------------
	w_fsmReset <= btnR or btnU;
	w_clkReset <= btnU or btnL;
    d_floor <= sw(3 downto 0);
    i_go <= btnC;
    o_activate <= i_go2;
    i_resetL <= o_destAc;
    

	-- ground unused LEDs (which is all of them for Minimum functionality)
	led(9 downto 6) <= (others => '0');

	-- leave unused switches UNCONNECTED
	
	-- Ignore the warnings associated with these signals
	-- Alternatively, you can create a different board implementation, 
	--   or make additional adjustments to the constraints file
	
	-- wire up active-low 7SD anodes (an) as required
	
	-- Tie any unused anodes to power ('1') to keep them off
	
	lights_proc : process (o_up_down, o_stop, c_floor)
	   begin
	       if (o_up_down = '0') and  (o_stop = '0') and (c_floor > "0001") then
                   w_up <= '0';
                   w_down <= '1';      
	       elsif (o_up_down = '1') and  (o_stop = '0') and (c_floor < "1111") then
                   w_up <= '1';
                   w_down <= '0';                   
	       else w_up <= '0';
	            w_down <= '0';	       
	       end if;	     
	end process;
	
	floors_proc : process (c_floor)
	   begin
	       if (c_floor > "1001") then
	           w_ones <= std_logic_vector(unsigned(c_floor) - "1010");
	           w_tens <= "0001";
	       
	       elsif (c_floor < "1010") then
               w_ones <= c_floor;
               w_tens <= "0000";
               
	       end if;
    end process;
    
    selector_proc : process (o_sel)
       begin
           if (o_sel = "1110") then
               an <= "0111";
               
           elsif (o_sel = "0111") then
               an <= "1011";   
                       
           elsif (o_sel = "1011") then
               an <= "0111";     
                     
           elsif (o_sel = "1101") then
               an <= "1011";
                      
           end if;
    end process;
    
    latch_proc : process (i_go, i_resetL)
        begin
           if (i_go = '1' and i_resetL = '0') then
               o_activate <= '1'; 
               
           --elsif (i_resetL = '1') then
               --o_activate <= '0';
            
            else o_activate <= '0';
            
        end if;
    end process;
    
    changefloor_proc : process (c_floor, d_floor)
        begin
            if (c_floor > d_floor and i_go2 = '1') then
                o_up_down <= '0';
                o_stop <= '0';
                o_destAc <= '0';
                         
            elsif (c_floor < d_floor and i_go2 = '1') then
                o_up_down <= '1';
                o_stop <= '0';
                o_destAc <= '0';
                
            elsif (c_floor = d_floor and i_go2 = '1') then
                o_up_down <= '0';
                o_stop <= '1';
                o_destAc <= '1';
                
            end if;
    end process;
	
end top_basys3_arch;
