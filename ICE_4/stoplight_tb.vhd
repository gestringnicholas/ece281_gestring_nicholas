--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : stoplight_tb.vhd (TEST BENCH)
--| AUTHOR(S)     : Maj Jeff Falkinburg and Capt Phillip Warner
--| CREATED       : Spring 2017 Modified: 03/05/2020 by Capt Dan Johnson
--| DESCRIPTION   : This file provides a solution testbench for the stoplight entity
--|
--| DOCUMENTATION : None
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : stoplight.vhd
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY stoplight_tb IS
END stoplight_tb;
 
ARCHITECTURE behavior OF stoplight_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
    COMPONENT stoplight
    PORT(
         C : IN  std_logic;
         reset : IN  std_logic;
         clk : IN  std_logic;
         R : OUT  std_logic;
         Y : OUT  std_logic;
         G : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal i_C : std_logic := '0';
   signal i_reset : std_logic := '0';
   signal i_clk : std_logic := '0';

 	--Outputs
   signal o_R : std_logic;
   signal o_Y : std_logic;
   signal o_G : std_logic;

   -- Clock period definitions
   constant k_clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: stoplight PORT MAP (
          C => i_C,
          reset => i_reset,
          clk => i_clk,
          R => o_R,
          Y => o_Y,
          G => o_G
        );

   -- Clock process definitions
   clk_proc : process
   begin
		i_clk <= '0';
		wait for k_clk_period/2;
		i_clk <= '1';
		wait for k_clk_period/2;
   end process;
  
  
   -- Stimulus process
   -- Use 220 ns for simulation
   sim_proc: process
   begin
        -- sequential timing		
		i_reset <= '1';
		wait for k_clk_period*1;
		
		i_reset <= '0';
		wait for k_clk_period*1;
		
		-- alternative way of implementing Finite State Machine Inputs
		-- starts after "wait for" statements
		-- statements after this one start in paralell to this one
		i_C <= '0', '1' after 40 ns, '0' after 80ns, '1' after 120 ns, '0' after 160 ns, '1' after 170 ns;

        -- one way to make using the reset easier would be to use a separate process to control it
        wait for k_clk_period*19;
        i_reset <= '1';

      wait;
   end process;

END;
